#ifndef Z_INC_SERIAL_H
#define Z_INC_SERIAL_H

#include <zephyr/types.h>
#include <zephyr/device.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#define RING_BUF_SIZE 1024
#define MAX_UART_SEND_SIZE 255

typedef int (*rx_process_t)(uint8_t *data, uint16_t len);

struct serial_inst
{
    const struct device *uart_dev;
    struct ring_buf tx_ring_buf;
    struct ring_buf rx_ring_buf;
    uint8_t tx_ring_buffer[RING_BUF_SIZE];
    uint8_t rx_ring_buffer[RING_BUF_SIZE];
    volatile bool tx_busy;
    struct k_sem rx_sem;
    rx_process_t rx_callback;

    int (*tx)(const uint8_t *data, uint16_t len);
};

struct serial_inst *serial_init(const struct device *uart_dev, rx_process_t rx_process_func);

#endif // Z_INC_SERIAL_H
