/**
 * for nrf52840dongle
 */
#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/usb/usbd.h>
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(cdc_acm_echo, LOG_LEVEL_DBG);

#include "serial.h"

static struct k_thread tx_thread_data;
static K_THREAD_STACK_DEFINE(tx_thread_stack, 1024);

// const struct device *const uart_dev = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);
static const struct device *uart_dev = DEVICE_DT_GET(DT_NODELABEL(uart0));

struct serial_inst *serial_inst = NULL;

static void serial_cb(const uint8_t *const data, uint16_t len)
{
	LOG_HEXDUMP_DBG(data, len, "RX");
}

static void tx_thread(void *unused1, void *unused2, void *unused3)
{
	ARG_UNUSED(unused1);
	ARG_UNUSED(unused2);
	ARG_UNUSED(unused3);

	while (1)
	{
		const char *msg = "Hello from nRF52840\n";
		serial_inst->tx((const uint8_t *)msg, strlen(msg));
		LOG_INF("send data !!!!!!!!!!!");
		k_msleep(10);
	}
}

int main(void)
{
	serial_inst = serial_init(uart_dev, (rx_process_t)serial_cb);
	if (!serial_inst)
	{
		LOG_ERR("Failed to init serial lib.");
		return -1;
	}

	LOG_INF("Serial lib example start.");

	// Start TX thread to produce tons of data to send
	k_thread_create(&tx_thread_data, tx_thread_stack, K_THREAD_STACK_SIZEOF(tx_thread_stack),
					tx_thread, NULL, NULL, NULL, 7, 0, K_NO_WAIT);

	return 0;
}
